//
//  ViewController.swift
//  TestMatch
//
//  Created by Dhawal Mahajan on 26/03/20.
//  Copyright © 2020 Dhawal Mahajan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var playerTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerTableviewCell", for: indexPath) as? PlayerTableviewCell {
            cell.playerName.text = "\(indexPath.row)"
            return cell
        }
        return UITableViewCell()
    }
    
    
}
