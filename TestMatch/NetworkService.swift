//
//  NetworkService.swift
//  TestMatch
//
//  Created by Dhawal Mahajan on 26/03/20.
//  Copyright © 2020 Dhawal Mahajan. All rights reserved.
//

import Foundation
let match = "https://cricket.yahoo.net/sifeeds/cricket/live/json/sapk01222019186652.json"
class NetworkService {
    static let shared = NetworkService()
    private init() {}
    
    func callNetworkServie(completion:@escaping (Match) ->Void) {
        guard let matchUrl = URL(string: match) else { return }
        URLSession.shared.dataTask(with: matchUrl) { (data, response, error) in
            guard let matchData = data else { return }
            do {
                let decoder = JSONDecoder()
                let decodeData = try decoder.decode(Match.self, from: matchData)
                if decodeData != nil {
                    completion(decodeData)
                }
            }
                catch {
                    print("error generated in decoding: ", error)
                }
            }.resume()
        }
}
