

import Foundation

// MARK: - Match
struct Match: Codable {
    let matchdetail: Matchdetail?
    let nuggets: [String]?
    let innings: [Inning]?
    let teams: [String: Team]?
    let notes: [String: [String]]?

    enum CodingKeys: String, CodingKey {
        case matchdetail = "Matchdetail"
        case nuggets = "Nuggets"
        case innings = "Innings"
        case teams = "Teams"
        case notes = "Notes"
    }
}



// MARK: - Inning
struct Inning: Codable {
    let number, battingteam, total, wickets: String?
    let overs, runrate, byes, legbyes: String?
    let wides, noballs, penalty, allottedOvers: String?
    let batsmen: [InningBatsman]?
    let partnershipCurrent: PartnershipCurrent?
    let bowlers: [Bowler]?
    let fallofWickets: [FallofWicket]?
    let powerPlay: PowerPlay?
    let target: String?

    enum CodingKeys: String, CodingKey {
        case number = "Number"
        case battingteam = "Battingteam"
        case total = "Total"
        case wickets = "Wickets"
        case overs = "Overs"
        case runrate = "Runrate"
        case byes = "Byes"
        case legbyes = "Legbyes"
        case wides = "Wides"
        case noballs = "Noballs"
        case penalty = "Penalty"
        case allottedOvers = "AllottedOvers"
        case batsmen = "Batsmen"
        case partnershipCurrent = "Partnership_Current"
        case bowlers = "Bowlers"
        case fallofWickets = "FallofWickets"
        case powerPlay = "PowerPlay"
        case target = "Target"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.inningBatsmanTask(with: url) { inningBatsman, response, error in
//     if let inningBatsman = inningBatsman {
//       ...
//     }
//   }
//   task.resume()

// MARK: - InningBatsman
struct InningBatsman: Codable {
    let batsman, runs, balls, fours: String?
    let sixes, dots, strikerate, dismissal: String?
    let howout, bowler, fielder: String?
    let isonstrike: Bool?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case runs = "Runs"
        case balls = "Balls"
        case fours = "Fours"
        case sixes = "Sixes"
        case dots = "Dots"
        case strikerate = "Strikerate"
        case dismissal = "Dismissal"
        case howout = "Howout"
        case bowler = "Bowler"
        case fielder = "Fielder"
        case isonstrike = "Isonstrike"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.bowlerTask(with: url) { bowler, response, error in
//     if let bowler = bowler {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Bowler
struct Bowler: Codable {
    let bowler, overs, maidens, runs: String?
    let wickets, economyrate, noballs, wides: String?
    let dots: String?
    let isbowlingtandem, isbowlingnow: Bool?
    let thisOver: [ThisOver]?

    enum CodingKeys: String, CodingKey {
        case bowler = "Bowler"
        case overs = "Overs"
        case maidens = "Maidens"
        case runs = "Runs"
        case wickets = "Wickets"
        case economyrate = "Economyrate"
        case noballs = "Noballs"
        case wides = "Wides"
        case dots = "Dots"
        case isbowlingtandem = "Isbowlingtandem"
        case isbowlingnow = "Isbowlingnow"
        case thisOver = "ThisOver"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.thisOverTask(with: url) { thisOver, response, error in
//     if let thisOver = thisOver {
//       ...
//     }
//   }
//   task.resume()

// MARK: - ThisOver
struct ThisOver: Codable {
    let t, b: String?

    enum CodingKeys: String, CodingKey {
        case t = "T"
        case b = "B"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.fallofWicketTask(with: url) { fallofWicket, response, error in
//     if let fallofWicket = fallofWicket {
//       ...
//     }
//   }
//   task.resume()

// MARK: - FallofWicket
struct FallofWicket: Codable {
    let batsman, score, overs: String?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case score = "Score"
        case overs = "Overs"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.partnershipCurrentTask(with: url) { partnershipCurrent, response, error in
//     if let partnershipCurrent = partnershipCurrent {
//       ...
//     }
//   }
//   task.resume()

// MARK: - PartnershipCurrent
struct PartnershipCurrent: Codable {
    let runs, balls: String?
    let batsmen: [PartnershipCurrentBatsman]?

    enum CodingKeys: String, CodingKey {
        case runs = "Runs"
        case balls = "Balls"
        case batsmen = "Batsmen"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.partnershipCurrentBatsmanTask(with: url) { partnershipCurrentBatsman, response, error in
//     if let partnershipCurrentBatsman = partnershipCurrentBatsman {
//       ...
//     }
//   }
//   task.resume()

// MARK: - PartnershipCurrentBatsman
struct PartnershipCurrentBatsman: Codable {
    let batsman, runs, balls: String?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case runs = "Runs"
        case balls = "Balls"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.powerPlayTask(with: url) { powerPlay, response, error in
//     if let powerPlay = powerPlay {
//       ...
//     }
//   }
//   task.resume()

// MARK: - PowerPlay
struct PowerPlay: Codable {
    let pp1, pp2: String?

    enum CodingKeys: String, CodingKey {
        case pp1 = "PP1"
        case pp2 = "PP2"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.matchdetailTask(with: url) { matchdetail, response, error in
//     if let matchdetail = matchdetail {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Matchdetail
struct Matchdetail: Codable {
    let teamHome, teamAway: String?
    let match: MatchClass?
    let series: Series?
    let venue: Venue?
    let officials: Officials?
    let weather, tosswonby, status, statusID: String?
    let playerMatch, result, winningteam, winmargin: String?
    let equation: String?

    enum CodingKeys: String, CodingKey {
        case teamHome = "Team_Home"
        case teamAway = "Team_Away"
        case match = "Match"
        case series = "Series"
        case venue = "Venue"
        case officials = "Officials"
        case weather = "Weather"
        case tosswonby = "Tosswonby"
        case status = "Status"
        case statusID = "Status_Id"
        case playerMatch = "Player_Match"
        case result = "Result"
        case winningteam = "Winningteam"
        case winmargin = "Winmargin"
        case equation = "Equation"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.matchClassTask(with: url) { matchClass, response, error in
//     if let matchClass = matchClass {
//       ...
//     }
//   }
//   task.resume()

// MARK: - MatchClass
struct MatchClass: Codable {
    let livecoverage, id, code, league: String?
    let number, type, date, time: String?
    let offset, daynight: String?

    enum CodingKeys: String, CodingKey {
        case livecoverage = "Livecoverage"
        case id = "Id"
        case code = "Code"
        case league = "League"
        case number = "Number"
        case type = "Type"
        case date = "Date"
        case time = "Time"
        case offset = "Offset"
        case daynight = "Daynight"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.officialsTask(with: url) { officials, response, error in
//     if let officials = officials {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Officials
struct Officials: Codable {
    let umpires, referee: String?

    enum CodingKeys: String, CodingKey {
        case umpires = "Umpires"
        case referee = "Referee"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.seriesTask(with: url) { series, response, error in
//     if let series = series {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Series
struct Series: Codable {
    let id, name, status, tour: String?
    let tourName: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case status = "Status"
        case tour = "Tour"
        case tourName = "Tour_Name"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.venueTask(with: url) { venue, response, error in
//     if let venue = venue {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Venue
struct Venue: Codable {
    let id, name: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.teamTask(with: url) { team, response, error in
//     if let team = team {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Team
struct Team: Codable {
    let nameFull, nameShort: String?
    let players: [String: Player]?

    enum CodingKeys: String, CodingKey {
        case nameFull = "Name_Full"
        case nameShort = "Name_Short"
        case players = "Players"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.playerTask(with: url) { player, response, error in
//     if let player = player {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Player
struct Player: Codable {
    let position, nameFull: String?
    let batting: Batting?
    let bowling: Bowling?
    let iscaptain, iskeeper: Bool?

    enum CodingKeys: String, CodingKey {
        case position = "Position"
        case nameFull = "Name_Full"
        case batting = "Batting"
        case bowling = "Bowling"
        case iscaptain = "Iscaptain"
        case iskeeper = "Iskeeper"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.battingTask(with: url) { batting, response, error in
//     if let batting = batting {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Batting
struct Batting: Codable {
    let style: Style?
    let average, strikerate, runs: String?

    enum CodingKeys: String, CodingKey {
        case style = "Style"
        case average = "Average"
        case strikerate = "Strikerate"
        case runs = "Runs"
    }
}

enum Style: String, Codable {
    case lhb = "LHB"
    case rhb = "RHB"
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.bowlingTask(with: url) { bowling, response, error in
//     if let bowling = bowling {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Bowling
struct Bowling: Codable {
    let style, average, economyrate, wickets: String?

    enum CodingKeys: String, CodingKey {
        case style = "Style"
        case average = "Average"
        case economyrate = "Economyrate"
        case wickets = "Wickets"
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

    func matchTask(with url: URL, completionHandler: @escaping (Match?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
