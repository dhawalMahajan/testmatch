//
//  PlayerTableviewCell.swift
//  TestMatch
//
//  Created by Dhawal Mahajan on 26/03/20.
//  Copyright © 2020 Dhawal Mahajan. All rights reserved.
//

import UIKit

class PlayerTableviewCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
